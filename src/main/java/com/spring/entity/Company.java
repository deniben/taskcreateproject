package com.spring.entity;

import com.spring.enums.CompanyType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Company {

    @Id
    private Long id;

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private CompanyType type;

    @Column(name = "mark_avg")
    private Long avgMark;

    @ManyToMany
    @JoinTable(name = "proposed_skills",
                    joinColumns = { @JoinColumn(name = "company_id") },
                    inverseJoinColumns = { @JoinColumn(name = "skill_id") })
    private Set<Skill> proposedSkills = new HashSet<>();

    @OneToMany(mappedBy = "company")
    private List<Profile> workers;

    @ManyToMany(mappedBy = "ownedCompanies")
    private Set<Profile> owners = new HashSet<>();

    public Company() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CompanyType getType() {
        return type;
    }

    public void setType(CompanyType type) {
        this.type = type;
    }

    public Long getAvgMark() {
        return avgMark;
    }

    public void setAvgMark(Long avgMark) {
        this.avgMark = avgMark;
    }

    public List<Profile> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Profile> workers) {
        this.workers = workers;
    }

    public Set<Profile> getOwners() {
        return owners;
    }

    public void setOwners(Set<Profile> owners) {
        this.owners = owners;
    }

    public Set<Skill> getProposedSkills() {
        return proposedSkills;
    }

    public void setProposedSkills(Set<Skill> proposedSkills) {
        this.proposedSkills = proposedSkills;
    }
}
