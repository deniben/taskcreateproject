package com.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Statement {

    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Company employee;

    @ManyToOne
    @JoinColumn(name = "employer_id")
    private Company employer;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    private String text;

    public Statement() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getEmployee() {
        return employee;
    }

    public void setEmployee(Company employee) {
        this.employee = employee;
    }

    public Company getEmployer() {
        return employer;
    }

    public void setEmployer(Company employer) {
        this.employer = employer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
