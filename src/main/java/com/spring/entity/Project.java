package com.spring.entity;

import com.spring.enums.ProjectStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Project {

    @Id
    private Long id;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private ProjectType projectType;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private Company companyCreator;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Company companyEmployee;

    @ManyToMany
    @JoinTable(name = "required_skills",
                joinColumns = { @JoinColumn(name = "project_id") },
                inverseJoinColumns = { @JoinColumn(name = "skill_id") })
    private Set<Skill> requiredSkills = new HashSet<>();

    private Double budget;

    private LocalDate expiryDate;

    @Enumerated(EnumType.STRING)
    private ProjectStatus status;

    private Long employeeMark;

    private Long employerMark;

    public Project() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectType(ProjectType projectType) {
        this.projectType = projectType;
    }

    public Company getCompanyCreator() {
        return companyCreator;
    }

    public void setCompanyCreator(Company companyCreator) {
        this.companyCreator = companyCreator;
    }

    public Company getCompanyEmployee() {
        return companyEmployee;
    }

    public void setCompanyEmployee(Company companyEmployee) {
        this.companyEmployee = companyEmployee;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public Long getEmployeeMark() {
        return employeeMark;
    }

    public void setEmployeeMark(Long employeeMark) {
        this.employeeMark = employeeMark;
    }

    public Long getEmployerMark() {
        return employerMark;
    }

    public void setEmployerMark(Long employerMark) {
        this.employerMark = employerMark;
    }

    public Set<Skill> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(Set<Skill> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }
}
