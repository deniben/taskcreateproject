package com.spring.dao;

import com.spring.dao.BaseDao;
import com.spring.entity.Project;

public interface ProjectDao extends BaseDao<Project> {

}
