package com.spring.dao;

import java.util.List;

public interface BaseDao<T> {

    List<T> findAll();

    void save(T t);

    void delete(long id);

    T findById(long id);
}
