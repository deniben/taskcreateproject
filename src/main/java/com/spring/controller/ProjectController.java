package com.spring.controller;

import com.spring.dao.ProjectDao;
import com.spring.entity.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProjectController {

    @Autowired
    private ProjectDao projectDao;

    @GetMapping("/projects")
    List<Project> findAll() {
        return projectDao.findAll();
    }

    @PostMapping("/projects")
    void save(@RequestBody Project project) {
        projectDao.save(project);
    }

    @GetMapping("/projects/{id}")
    Project findById(@PathVariable Long id) {
        return projectDao.findById(id);
    }

    @DeleteMapping("/projects/{id}")
    void delete(@PathVariable Long id) {
        projectDao.delete(id);
    }
}
