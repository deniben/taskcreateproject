package com.spring.repository;

import com.spring.entity.User;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface UserRepository {
    List<User> findAll();
    void save (User user);
    void delete(long id);
    User findById(long id);
    User findByUsername(String username);

}
