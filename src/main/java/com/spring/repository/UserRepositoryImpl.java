package com.spring.repository;

import com.spring.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<User> findAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = sessionFactory.openSession().getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        CriteriaQuery<User> allUsers = query.select(root);
        TypedQuery<User> allQuery = sessionFactory.openSession().createQuery(allUsers);
        return allQuery.getResultList();

    }

    @Override
    public User findByUsername(String username) {
            Session session = sessionFactory.getCurrentSession();

            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

            CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

            Root<User> userRoot = criteriaQuery.from(User.class);

            criteriaQuery.select(userRoot).where(criteriaBuilder.equal(userRoot.get("username"), username));

            List<User> result = session.createQuery(criteriaQuery).getResultList();

            if (result != null && result.size() > 0) {

                return result.get(0);

            }

            return null;

    }

    @Override
    public void save(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    @Override
    public void delete(long id) {
        sessionFactory.getCurrentSession().delete(findById(id));
    }

    @Override
    public User findById(long id) {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> user = query.from(User.class);
        CriteriaQuery<User> result = query.select(user).where(criteriaBuilder.and(criteriaBuilder.equal(user.get("id"), id)));
        TypedQuery<User> us = sessionFactory.getCurrentSession().createQuery(result);
        return us.getSingleResult();
    }

}
