package com.spring.repository;

import com.spring.entity.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {
    @Autowired
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Project> findAll() {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Project> query = criteriaBuilder.createQuery(Project.class);
        Root<Project> root = query.from(Project.class);
        CriteriaQuery<Project> allProjects = query.select(root);
        TypedQuery<Project> allQuery = sessionFactory.getCurrentSession().createQuery(allProjects);
        return allQuery.getResultList();
    }

    @Override
    public void save(Project project) {
        sessionFactory.getCurrentSession().saveOrUpdate(project);
    }

    @Override
    public void delete(Long id) {
        sessionFactory.getCurrentSession().delete(findById(id));
    }

    @Override
    @Transactional
    public Project findById(Long id) {
        CriteriaBuilder criteriaBuilder = sessionFactory.getCurrentSession().getCriteriaBuilder();
        CriteriaQuery<Project> query = criteriaBuilder.createQuery(Project.class);
        Root<Project> projectRoot = query.from(Project.class);
        CriteriaQuery<Project> result = query.select(projectRoot).where(criteriaBuilder.equal(projectRoot.get("id"), id));
        TypedQuery<Project> us = sessionFactory.getCurrentSession().createQuery(result);
        return us.getSingleResult();
    }
}
