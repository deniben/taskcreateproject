package com.spring.repository;

import com.spring.entity.Project;

import java.util.List;

public interface ProjectRepository {
    List<Project> findAll();
    void save(Project project);
    void delete(Long id);
    Project findById(Long id);


}
