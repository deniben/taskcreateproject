package com.spring.service;

import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class PropertiesService {

    public Map<String, String> getConnectionProperties() {

        InputStream inputStream = PropertiesService.class.getResourceAsStream("/application.properties");

        Properties properties = new Properties();

        try {

            properties.load(inputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> propMap = new HashMap<>();

        propMap.put("url", properties.getProperty("jpa.url"));
        propMap.put("username", properties.getProperty("jpa.username"));
        propMap.put("password", properties.getProperty("jpa.password"));
        propMap.put("driver", properties.getProperty("jpa.driver"));

        return propMap;

    }

    public Map<String, String> getHibernateProperties() {


        InputStream inputStream = PropertiesService.class.getResourceAsStream("/application.properties");

        Properties properties = new Properties();

        try {

            properties.load(inputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> propMap = new HashMap<>();

        propMap.put("ddl-auto", properties.getProperty("hibernate.hbm2ddl.auto"));
        propMap.put("dialect", properties.getProperty("hibernate.dialect"));

        return propMap;

    }

}
