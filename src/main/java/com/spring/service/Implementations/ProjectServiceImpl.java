package com.spring.service.Implementations;

import com.spring.dao.ProjectDao;
import com.spring.entity.Project;
import com.spring.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDao projectDao;

    @Override
    public List<Project> findAll() {
        return projectDao.findAll();
    }

    @Override
    public ResponseEntity<Object> save(Project project) {
        try {
            projectDao.save(project);
            return ResponseEntity.status(HttpStatus.OK).body(project);
        } catch (Exception exc) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @Override
    public ResponseEntity<Object> delete(long id) {
    Project project = findById(id);
        if (project != null) {
            projectDao.delete(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @Override
    public Project findById(long id) {
        return projectDao.findById(id);
    }
}
