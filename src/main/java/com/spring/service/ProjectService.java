package com.spring.service;

import com.spring.entity.Project;
import org.springframework.http.ResponseEntity;

;
import java.util.List;

public interface ProjectService {
    public List<Project> findAll();
    public ResponseEntity<Object> save(Project project);
    public ResponseEntity<Object> delete(long id);
    public Project findById(long id);
}
